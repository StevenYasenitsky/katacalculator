import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String operation  = s.nextLine();
        System.out.println(calc(operation));
    }

    public static String calc(String input) {
        input = input.replace(" ", "");
        List<String> ops = List.of("/", "*", "+", "-");
        List<String> exp = Arrays.asList(input.split(""));
        List<String> opList = exp.stream().filter(ops::contains).toList();
        if (opList.size() != 1) {
            throw new RuntimeException("������ �� �������� �������������� ���������: ����������� �������� ��� ����������� �������� ��� ����� ���������� ���������.");
        }
        String op = opList.get(0);
        String[] arr = input.split(op.equals("+") ? "\\+" : op.equals("*") ? "\\*" : op);
        exp = Arrays.asList(arr);
        List<String> nums = exp.stream().filter(el -> !op.contains(el)).toList();
        List<String> romeNums = List.of("I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", "XXI", "XXII", "XXIII", "XXIV", "XXV", "XXVI", "XXVII", "XXVIII", "XXIX", "XXX", "XXXI", "XXXII", "XXXIII", "XXXIV", "XXXV", "XXXVI", "XXXVII", "XXXVIII", "XXXIX", "XL", "XLI", "XLII", "XLIII", "XLIV", "XLV", "XLVI", "XLVII", "XLVIII", "XLIX", "L", "LI", "LII", "LIII", "LIV", "LV", "LVI", "LVII", "LVIII", "LIX", "LX", "LXI", "LXII", "LXIII", "LXIV", "LXV", "LXVI", "LXVII", "LXVIII", "LXIX", "LXX", "LXXI", "LXXII", "LXXIII", "LXXIV", "LXXV", "LXXVI", "LXXVII", "LXXVIII", "LXXIX", "LXXX", "LXXXI", "LXXXII", "LXXXIII", "LXXXIV", "LXXXV", "LXXXVI", "LXXXVII", "LXXXVIII", "LXXXIX", "XC", "XCI", "XCII", "XCIII", "XCIV", "XCV", "XCVI", "XCVII", "XCVIII", "XCIX", "C");
        Map<String, Integer> arabLatinNumsList = new HashMap<>();
        for (int i = 1; i < 101; i++) {
            arabLatinNumsList.put(romeNums.get(i - 1), i);
        }
        int realNum1;
        int realNum2;
        int result = 0;
        boolean romeSituation = arabLatinNumsList.containsKey(nums.get(0)) && arabLatinNumsList.containsKey(nums.get(1));
        if (romeSituation) {
            realNum1 = romeNums.indexOf(nums.get(0)) + 1;
            realNum2 = romeNums.indexOf(nums.get(1)) + 1;
        } else {
            try {
                if (arabLatinNumsList.containsKey(nums.get(0)) || arabLatinNumsList.containsKey(nums.get(1))) {
                    throw new RuntimeException("������������ ������������ ������ ������� ���������");
                }
                realNum1 = Integer.parseInt(nums.get(0));
                realNum2 = Integer.parseInt(nums.get(1));
                if (!(realNum1 > 0 && realNum1 < 11 & realNum2 > 0 && realNum2 < 11)) {
                    throw new RuntimeException("��� �������� ������������� �����, �� ���� �� ��� ��� ��� ������ 1 ��� ������ 10");
                }
            } catch (NumberFormatException e) {
                throw new RuntimeException("�������� � ������������ ��������� � �����");
            }
        }
        switch (op) {
            case "/":
                result = (int) (Math.ceil(realNum1 / (double) realNum2));
                break;
            case "*":
                result = realNum1 * realNum2;
                break;
            case "-":
                result = realNum1 - realNum2;
                break;
            case "+":
                result = realNum1 + realNum2;
                break;
        }
        String res = "";
        if (romeSituation && result > 0) {
            for (var el : arabLatinNumsList.entrySet()
            ) {
                if (el.getValue() == result) {
                    res = el.getKey();
                    break;
                }
            }
        } else if (romeSituation) {
            throw new RuntimeException("������, �.�. ������������� ������� ���� �� ������");
        } else {
            res = String.valueOf(result);
        }
        return res;
    }
}